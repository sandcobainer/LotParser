import os,sys
import json
from bs4 import BeautifulSoup
direc='./data/2017-12-20/'
files = [f for f in os.listdir('./data/2017-12-20/') if os.path.isfile(os.path.join('./data/2017-12-20/',f))]
# scan all files in

mainarray=[]
for f in files:  # loop in
	if (f[-4:]=='html'):
		artdict={}
		filedir=direc+f
		print(filedir)
		soup=BeautifulSoup(open(filedir),'html.parser') #brew the soup
		artdict['artist']=soup.find("h3","artist").string
		print(artdict['artist'])

		worksdic={}                                     #building works array
		innerdict={}

		innerdict['title']=soup.body.h3.string          #use dictionary to get artists and works and build array of objects
		currency=soup.find("span",{'class':'currency'}).string
		amount=soup.find_all("span")[1].string
		amount=int(amount.replace(",",""))
		print(currency)
		print(amount)
		if(currency=='GBP'):
			amount=amount/1.34                      #convert to usd from gbp
		innerdict['currency']='USD'                           #get currency and amount and insert into innerdict
		innerdict['amount']=amount

		innerdictarr=[]
		innerdictarr.append(innerdict)
		worksdic['works']=innerdictarr
		print(worksdic)                                 #make larger works dictionary

		mainarray.append(artdict)
		mainarray.append(worksdic)                      #finally append everything into mainarray

jsonarray=json.dumps(mainarray)
print(jsonarray)



#note: i'm not pushing works that belong to the same artist into one single "works" array due to time constraints.
#instead building a "jsonarray" row for each .html page
#group by would be better to do on a database or on a flattened pandas dataframe