import os
import bs4,sys
from bs4 import BeautifulSoup
direc='./data/2015-03-18/'
files = [f for f in os.listdir('./data/2015-03-18/') if os.path.isfile(os.path.join('./data/2015-03-18/',f))]
print(files)
# scan all files in

mainarray=[]
for f in files: # loop in
	if (f[-4:]=='html'):
		dict={}
		filedir=direc+f
		soup=BeautifulSoup(open(filedir),'html.parser')
		dict['artist']=soup.body.h2.string
		dict['works']=soup.body.h3.string  #use dictionary to get artists and works and build array of objects
		mainarray.append(dict)

print(mainarray)
