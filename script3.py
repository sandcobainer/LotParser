import os,sys
import json
from bs4 import BeautifulSoup
direc='./data/2015-03-18/'
files = [f for f in os.listdir('./data/2015-03-18/') if os.path.isfile(os.path.join('./data/2015-03-18/',f))]
# scan all files in

mainarray=[]
for f in files:  # loop in
	if (f[-4:]=='html'):
		artdict={}
		filedir=direc+f
		soup=BeautifulSoup(open(filedir),'html.parser') #brew the soup
		artdict['artist']=soup.body.h2.string

		worksdic={}                                    #building works array
		innerdict={}

		innerdict['title']=soup.body.h3.string       #use dictionary to get artists and works and build array of objects
		innerdict['price']=soup.find_all("div")[1].string
		innerdictarr=[]
		innerdictarr.append(innerdict)
		worksdic['works']=innerdictarr
		print(worksdic)                                #make larger works dictionary

		mainarray.append(artdict)
		mainarray.append(worksdic)                      #finally append everything into mainarray

jsonarray=json.dumps(mainarray)
print(jsonarray)
